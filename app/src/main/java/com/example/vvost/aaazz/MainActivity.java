package com.example.vvost.aaazz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {
    private TextView tvData;
    StringBuffer buffer;
    DatabaseReference databaseReference;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        buffer = new StringBuffer();
        FirebaseApp.initializeApp(this);
        tvData = (TextView) findViewById(R.id.textView);
        final TextView aaa = (TextView) findViewById(R.id.textView2);
      //  FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference();
        DatabaseReference referencee = databaseReference1.child("gerasjoss").child("foods");
        referencee.orderByValue().equalTo("2")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot data : dataSnapshot.getChildren()){
                            Model model = data.getValue(Model.class);
                            String name = model.getName();
                            tvData.setText(name);
                        }

                        // do whatever with product
                        // }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        tvData.setText("notworking");

                    }
                });
    }
}

